from DBapi import *

# create api object
dba = RRDSapi("root", "7u3WWqOlpp?m.7-F")
DB = dba.connect('romrep')
# set path of schemas
dba.setSchemaPath("schemas")

# create researchers for testing
# create researchers
try:
	researcherColl = DB.AQLQuery('INSERT {"_key": "12687537", "name": "Zuzana Bodnárová", "function": "researcher", "membership": "Group 3" } IN researcher', batchSize=100, rawResults=True)
except:
	pass
try:
	researcherColl = DB.AQLQuery('INSERT {"_key": "12687536", "name": "Jakob Wiedner", "function": "researcher", "membership": "Group 1" } IN researcher', batchSize=100, rawResults=True)
except:
	pass

# testing new database adding
def testAddData():
	input1 = {
		"metadata/name": "Szolnok Lakatos Interview",
		"metadata/description": "recording with Melinda and Gergely in Feb 21",
		"metadata/type": "rawData",
		"metadata/dateTime": "2021-02-22T14:51:50.00Z",
		"metadata/placeOfRecording": "Szolnok",
		"metadata/accessPermission": "private",
		"newConsultant": [
			{
				"consultant/name": "Melinda Lakatos",
				"consultant/dateOfBirth": "1985-07-24",
				"consultant/languagesSpoken": [
					"English",
					"Romani",
					"Hungarian"
				],
				"uploadConsent": "lknpj89ljlij._tmp"
			},
			{
				"consultant/name": "Gergely Lakatos",
				"consultant/languagesSpoken": [
					"German",
					"Romani",
					"Hungarian",
					"Romanian"
				],
				"uploadConsent": "sdkhfsdfuh._tmp"
			}		
		],
		"uploadFile": "xxxdjasdfjdaspiojf789h._tmp"	
	}
	res = dba.addNew("researcher/12687537", input1, "metadata")
	print(res)
testAddData()

# testing new database adding
def testAddData2():
	input2 = {
		"metadata/name": "Göteborg interview",
		"metadata/description": "recording with Sven",
		"metadata/type": "rawData",
		"metadata/dateTime": "2021-04-22T14:13:30.00Z",
		"metadata/placeOfRecording": "Göteborg",
		"metadata/accessPermission": "private",
		"newConsultant": [
			{
				"consultant/name": "Sven Bomvollen",
				"consultant/dateOfBirth": "1983-02-25",
				"consultant/languagesSpoken": [
					"English",
					"Romani",
					"Hungarian"
				],
				"uploadConsent": "lknpj89ljlij._tmp"
			}		
		],
		"uploadFile": "xxxdjasdfjdaspiojf789h._tmp"	
	}
	res = dba.addNew("researcher/12687536", input2, "metadata")
	print(res)
#testAddData2()

# test adding extraction
def testAddExtract():
	input3 = {
		"metadata/name": "extract melinda",
		"metadata/description": "extraction of part with melinda",
		"metadata/type": "editedData",
		"metadata/accessPermission": "open",
		"sourceFile": "metadata/16826373",
		"uploadFile": "xxxdjasdfjdaspiojf789h._tmp",
		"existingConsultant": [
			{
				"consultant": "consultant/16826375"
			} 
		]
	}
	res = dba.addNew("researcher/12687537", input3, "metadata")
	print(res)
#testAddExtract()

# test adding extraction
def testAddExtract2():
	input4 = {
		"metadata/name": "extract sven",
		"metadata/description": "extraction of part with sven",
		"metadata/type": "editedData",
		"metadata/accessPermission": "open",
		"sourceFile": "metadata/16584528",
		"uploadFile": "xxxdjasdfjdaspiojf789h._tmp",
		"existingConsultant": [
			{
				"consultant": "consultant/16584530"
			} 
		]
	}
	res = dba.addNew("researcher/12687536", input4, "metadata")
	print(res)
#testAddExtract2()

# test get existing consultant document list
#res = dba.getExistingConsultants("researcher/12687537")
#print(res)

# test get existing consultant document list
#res = dba.getExistingFiles("researcher/12687537")
#print(res)

# test get all metadata
#res = dba.query("researcher/12687537", {"consultant/languagesSpoken": ["Hungarian", "German"]}, "metadata")
#print(json.dumps(res, indent=4, sort_keys=True))

# test get single metadata
#res = dba.getDataById("researcher/12687537", "metadata/16826564", "metadata")
#print(json.dumps(res, indent=4, sort_keys=True))

# test value editing
#res = dba.editDocumentValue("researcher/12687537", "metadata", "metadata", "metadata/16638172", "name", "passage Melinda")
#print(res)

# test array adding
#res = dba.addToArray("researcher/12687537", "metadata", "consultants", "consultant/16579926", "languagesSpoken", "Serbian")
#print(res)

# test array removal
#res = dba.removeFromArray("researcher/12687536", "metadata", "consultants", "consultant/16579926", "languagesSpoken", "Serbian")
#print(res)

# test document removal
#res = dba.removeDocumentById("researcher/12687536", "metadata", "consultants", "consultant/16826379")
#print(res)

# test get linkable document list
#res = dba.getLinkableDocuments("researcher/12687537", "project", "linkedMetadata")
#print(res)

# test link documents
#res = dba.linkDocuments("researcher/12687537", "metadata", "parent", "metadata/16584490", "metadata/16584528")
#print(res)

# test unlink documents
#res = dba.unlinkDocuments("researcher/12687537", "metadata", "consultants", "appearsIn/16584501")
#print(res)

# test add consultant
def testAddConsultant():
	inputConsultant = {
		"consultant/name": "Šaban Bajramović",
		"consultant/dateOfBirth": "1936-04-16",
		"consultant/residence": "Niš",
		"consultant/languagesSpoken": [
			"Romani",
			"Serbian"
		],
		"uploadConsent": "lknpj89ljlij._tmp"
	}
	res = dba.addNew("researcher/12687536", inputConsultant, "consultant")
	print(res)
#testAddConsultant("researcher/12687537")

# test all consultants
#res = dba.query("researcher/12687537", {}, "consultant")
#print(json.dumps(res, indent=4, sort_keys=True))

# test add project
def testAddProject():
	inputProject = {
		"project/name": "Performative usage of Romani",
		"project/description": "Exploring how Romani is used to perform ethnic differentiation"
	}
	res = dba.addNew("researcher/12687537", inputProject, "project")
	print(res)
#testAddProject()

# test query projects
#res = dba.query("researcher/12687537", {}, "project")
#print(json.dumps(res, indent=4, sort_keys=True))

# test query single project
#res = dba.getDataById("researcher/12687537", "project/16585864", "project")
#print(json.dumps(res, indent=4, sort_keys=True))

# test project edit
#res = dba.editDocumentValue("researcher/12687537", "project", "projectModerated", "project/16585864", "name", "TESTO")
#print(res)

# test invite researcher
#res = dba.linkDocuments("researcher/12687537", "project", "invitedResearchers", "project/16585864", "researcher/12687536")
#print(res)

# test add metadata
#res = dba.linkDocuments("researcher/12687537", "project", "linkedMetadata", "project/16585864", "metadata/16638172")
#print(res)

# test add metadata
#res = dba.linkDocuments("researcher/12687537", "project", "linkedMetadata", "project/16585864", "metadata/16640192")
#print(res)

# test remove metadata
#res = dba.unlinkDocuments("researcher/12687537", "project", "linkedMetadata", "analyses/16824179")
#print(res)

# test add common note
def testAddCommonNote():
	inputProject = {
		"commonNote/title": "Draft paper 2",
		"commonNote/content": """
		{
      "ops": [
        {
        	"insert": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam ullamcorper sollicitudin viverra. Morbi tempus risus quis arcu luctus mollis. Duis risus elit, ultricies sed est vitae, viverra elementum magna. Cras non venenatis elit, id cursus leo. Praesent commodo augue venenatis urna euismod aliquet. Curabitur in quam rutrum, tincidunt sapien vitae, pulvinar nisl. Praesent eget augue purus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce vestibulum ut ligula eu sodales. Maecenas eget neque nulla.\nNam mattis in velit vel mattis. Nullam sollicitudin accumsan justo. Nunc porttitor scelerisque condimentum. Aliquam elementum, metus id tristique laoreet, mauris tortor aliquam ex, in ornare velit nulla quis felis. Proin in metus odio. Aliquam nec mi bibendum, posuere dui eget, semper enim. In consectetur libero in elit blandit consectetur. Pellentesque venenatis, magna ac interdum posuere, sem enim maximus urna, et elementum enim odio ac orci. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Aenean eget quam imperdiet, tincidunt tortor et, volutpat orci. Nulla sed enim sit amet leo luctus egestas faucibus nec nisi. Aenean vulputate viverra accumsan.\n"
        }
      ]
    }
		"""
	}
	res = dba.addNewCommonNote("researcher/12687536", "project/16585864", inputProject)
	print(res)
#testAddCommonNote()

# test listing common notes
#res = dba.queryCommonNotes("researcher/12687536", "project/16585864")
#print(json.dumps(res, indent=4, sort_keys=True))

# test get single common note
#res = dba.getDataById("researcher/12687536", "commonNote/16772951", "commonNote")
#print(json.dumps(res, indent=4, sort_keys=True))

# test edit consultant
#res = dba.editDocumentValue("researcher/12687537", "consultant", "consultants", "consultant/16584492", "residence", "Budapest")
#print(res)

# test edit commonNote
#res = dba.editDocumentValue("researcher/12687537", "commonNote", "commonNote", "commonNote/16772951", "content", """
#		{
#      "ops": [
#        {
#        	"insert": "Ich mag Pesto!"
#        }
#      ]
#    }
#		""", "81CB89Z6")
#print(res)
