from DBhandler import *

"""
Copyright 2021 Jakob Wiedner

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

class RRDSapi(RRDShandler):
	"""Back end wrapper class for RomRep data storage communication
	
	"""
	
	def addNew(self, researcherId, inputJSON, dataType):
		"""Add new data
		
		Add a new JSON object to the database.
		
		Args:
			researcherId: the ID of the researcher currently signed in
			inputJSON: the JSON object created according to the input schema			
			dataType: "metadata", "project", "consultant"
		Returns:
			True if successful and an error message otherwise
		"""
		try:
			self.setParams({"researcherId": researcherId})
			if dataType == "metadata":
				self.processData(inputJSON, 'processors/setMetadata.process.json', 'setMetadata.schema.json', ['metadata.schema.json', 'consultant.schema.json', 'consentForm.schema.json'], "RomRepDataStorage")
			elif dataType == "project":
				self.processData(inputJSON, 'processors/setProject.process.json', 'setProject.schema.json', ['project.schema.json'], "RomRepDataStorage")
			elif dataType == "consultant":
				self.processData(inputJSON, 'processors/setConsultant.process.json', 'setConsultant.schema.json', ['consultant.schema.json', 'consentForm.schema.json'], "RomRepDataStorage")
			else:
				return "404 Bad data type"
			return True
		except Exception as e:
			return e
	
	def getExistingConsultants(self, researcherId):
		"""Get a list of existing consultants
		
		Retrieve a list of all consultants linked to the researcher including the consultant's name, date of birth and residence.
		
		Args:
			researcherId: the ID of the researcher currently signed in
		
		Returns:
			A list of documents including the document ID and the consultant's name, date of birth and residence, if no success an error message is returned
		"""
		try:
			self.setParams({"researcherId": researcherId})
			result = self.getDocumentListFromSchema('setMetadata', 'existingConsultant.consultant')
			return result
		except Exception as e:
			return e
	
	def getExistingFiles(self, researcherId):
		"""Get a list of existing data files
		
		Retrieve a list of all data files linked to the researcher including the file name, date of recording and place of recording.
		
		Args:
			researcherId: the ID of the researcher currently signed in
		
		Returns:
			A list of documents including the document ID and the file name, date of recording and place of recording, if no success an error message is returned
		"""
		try:
			self.setParams({"researcherId": researcherId})
			result = self.getDocumentListFromSchema('setMetadata', 'sourceFile')
			return result
		except Exception as e:
			return e
	
	def query(self, researcherId, inputJSON, dataType):
		"""Get a list of matching data
		
		Retrieve a list of data structures from the database that match the query. 
		
		Args:
			researcherId: the ID of the researcher currently signed in
			inputJSON: the JSON object created according to the query input schema			
			dataType: "metadata", "project", "consultant"
		
		Returns:
			A result object holding the document IDs found and additional information for displaying, and an error message otherwise
		"""
		try:
			self.setParams({"researcherId": researcherId})
			if dataType == "metadata":
				result = self.processData(inputJSON, 'processors/queryAllMetadata.process.json', 'queryMetadata.schema.json', ['metadata.schema.json', 'consultant.schema.json', 'consentForm.schema.json'], "RomRepDataStorage")
			elif dataType == "project":
				result = self.processData(inputJSON, 'processors/queryAllProjects.process.json', 'queryProjects.schema.json', ['project.schema.json'], "RomRepDataStorage")
			elif dataType == "consultant":
				result = self.processData(inputJSON, 'processors/queryAllConsultants.process.json', 'empty.schema.json', ['consultant.schema.json', 'consentForm.schema.json'], "RomRepDataStorage")
			else:
				return "404 Bad data type"
			return result
		except Exception as e:
			return e
	
	def getDataById(self, researcherId, documentId, dataType):
		"""Get a single data item by ID
		
		Gets the document data together with linked documents.
		
		Args:
			researcherId: the ID of the researcher currently signed in
			documentId: the ID of the document requested
			dataType: "metadata", "project", "commonNote"
		
		Returns:
			A result object holding the data and all additional information, if no success an error message is returned and an error message otherwise		
		"""
		try:
			if dataType == "metadata":
				self.setParams({"researcherId": researcherId, "metadataId": documentId});
				result = self.processData({}, 'processors/queryMetadata.process.json', 'empty.schema.json', ['metadata.schema.json', 'consultant.schema.json', 'consentForm.schema.json'], "RomRepDataStorage")
			elif dataType == "project":
				self.setParams({"researcherId": researcherId, "projectId": documentId, "allowedData": ["editedData", "transcription", "note", "extraction"], "allowedPermission": ["RomRepOnly", "open"]})
				result = self.processData({}, 'processors/queryProject.process.json', 'empty.schema.json', ['project.schema.json', 'researcher.schema.json', 'metadata.schema.json', 'commonNote.schema.json'], "RomRepDataStorage")
			elif dataType == "commonNote":
				self.setParams({"researcherId": researcherId, "commonNoteId": documentId});
				result = self.processData({}, 'processors/queryCommonNote.process.json', 'empty.schema.json', ['commonNote.schema.json'], "RomRepDataStorage")
			else:
				return "404 Bad data type"
			return result
		except Exception as e:
			return e
	
	def editDocumentValue(self, researcherId, dataType, editGroup, documentId, propertyPath, newValue, checksum=None):
		"""Change the value of a property in a document
		
		Checks the edit group definition and replaces the existing value of the property against the new one given if valid.
		
		Args:
			researcherId: the ID of the researcher currently signed in
			dataType: "metadata", "project", "consultant", "commonNote"
			editGroup: the name of the edit group as defined in the result object
			documentId: the ID of the document holding the property as defined in the result object
			propertyPath: the path to the property to be changed as defined in the result object
			newValue: the new value to replace the old one with
			checksum: to be added when of value type 'long-text' (None by default)
			
		Returns:
			True if successful or an error message otherwise
		"""
		try:
			self.setParams({"researcherId": researcherId})
			if dataType == "metadata":
				self.loadSchemasFromFile(['consentForm.schema.json', 'metadata.schema.json', 'consultant.schema.json'])
				processPath = "processors/queryMetadata.process.json"
			elif dataType == "project":
				self.loadSchemasFromFile(['project.schema.json', 'commonNote.schema.json', 'metadata.schema.json', 'researcher.schema.json'])
				processPath = "processors/queryProject.process.json"
			elif dataType == "consultant":
				self.loadSchemasFromFile(['consultant.schema.json', 'consentForm.schema.json'])
				processPath = "processors/queryAllConsultants.process.json"
			elif dataType == "commonNote":
				self.loadSchemasFromFile(['commonNote.schema.json'])
				processPath = "processors/queryCommonNote.process.json"
			else:
				return "404 Bad data type"
			self.editValue(processPath, editGroup, documentId, propertyPath, newValue, None, checksum)
			return True
		except Exception as e:
			return e
	
	def addToArray(self, researcherId, dataType, editGroup, documentId, propertyPath, newValue):
		"""Add a value to an array property in a document
		
		Checks the edit group definition and add the new value to an array if valid.
		
		Args:
			researcherId: the ID of the researcher currently signed in
			dataType: "metadata", "project", "consultant", "commonNote"
			editGroup: the name of the edit group as defined in the result object
			documentId: the ID of the document holding the property as defined in the result object
			propertyPath: the path to the property to be changed as defined in the result object
			newValue: the new value to add
		
		Returns:
			True if successful or an error message otherwise
		"""
		try:
			self.setParams({"researcherId": researcherId})
			if dataType == "metadata":				
				self.loadSchemasFromFile(['consentForm.schema.json', 'metadata.schema.json', 'consultant.schema.json'])
				processPath = 'processors/queryMetadata.process.json'
			elif dataType == "project":
				self.loadSchemasFromFile(['project.schema.json', 'commonNote.schema.json', 'metadata.schema.json', 'researcher.schema.json'])
				processPath = "processors/queryProject.process.json"
			elif dataType == "consultant":
				self.loadSchemasFromFile(['consultant.schema.json', 'consentForm.schema.json'])
				processPath = "processors/queryAllConsultants.process.json"
			elif dataType == "commonNote":
				self.loadSchemasFromFile(['commonNote.schema.json'])
				processPath = "processors/queryCommonNote.process.json"
			else:
				return "404 Bad data type"
			self.addToList(processPath, editGroup, documentId, propertyPath, newValue)
			return True
		except Exception as e:
			return e
	
	def removeFromArray(self, researcherId, dataType, editGroup, documentId, propertyPath, value):
		"""Remove a value from an array property in a document
		
		Checks the edit group definition and remove the new value from an array if valid.
		
		Args:
			researcherId: the ID of the researcher currently signed in
			dataType: "metadata", "project", "consultant", "commonNote"
			editGroup: the name of the edit group as defined in the result object
			documentId: the ID of the document holding the property as defined in the result object
			propertyPath: the path to the property to be changed as defined in the result object
			value: the value to remove
		
		Returns:
			True if successful or an error message otherwise
		"""
		try:
			self.setParams({"researcherId": researcherId})
			if dataType == "metadata":				
				self.loadSchemasFromFile(['consentForm.schema.json', 'metadata.schema.json', 'consultant.schema.json'])
				processPath = 'processors/queryMetadata.process.json'
			elif dataType == "project":
				self.loadSchemasFromFile(['project.schema.json', 'commonNote.schema.json', 'metadata.schema.json', 'researcher.schema.json'])
				processPath = "processors/queryProject.process.json"
			elif dataType == "consultant":
				self.loadSchemasFromFile(['consultant.schema.json', 'consentForm.schema.json'])
				processPath = "processors/queryAllConsultants.process.json"
			elif dataType == "commonNote":
				self.loadSchemasFromFile(['commonNote.schema.json'])
				processPath = "processors/queryCommonNote.process.json"
			else:
				return "404 Bad data type"
			self.removeFromList(processPath, editGroup, documentId, propertyPath, value)
			return True
		except Exception as e:
			return e
	
	def removeDocumentById(self, researcherId, dataType, editGroup, documentId):
		"""Remove a document
		
		Checks the edit group definition and removes the document and all related documents having no other edges to third notes.
		
		Args:
			researcherId: the ID of the researcher currently signed in
			dataType: "metadata", "project", "consultant", "commonNote"
			editGroup: the name of the edit group as defined in the result object
			documentId: the ID of the document holding the property as defined in the result object
		
		Returns:
			True if successful or an error message otherwise
		"""
		try:
			self.setParams({"researcherId": researcherId});
			if dataType == "metadata":				
				self.loadSchemasFromFile(['consentForm.schema.json', 'metadata.schema.json', 'consultant.schema.json'])
				processPath = 'processors/queryMetadata.process.json'
			elif dataType == "project":
				self.loadSchemasFromFile(['project.schema.json', 'commonNote.schema.json', 'metadata.schema.json', 'researcher.schema.json'])
				processPath = "processors/queryProject.process.json"
			elif dataType == "consultant":
				self.loadSchemasFromFile(['consultant.schema.json', 'consentForm.schema.json'])
				processPath = "processors/queryAllConsultants.process.json"
			elif dataType == "commonNote":
				self.loadSchemasFromFile(['commonNote.schema.json'])
				processPath = "processors/queryCommonNote.process.json"
			else:
				return "404 Bad data type"
			self.removeDocument(processPath, editGroup, documentId)
			return True
		except Exception as e:
			return e
	
	def getLinkableDocuments(self, researcherId, dataType, editGroup):
		"""Get a list of linkable documents
		
		Checks the edit group definition and returns a list of document IDs and related data.
		
		Args:
			researcherId: the ID of the researcher currently signed in
			dataType: "metadata", "project", "consultant", "commonNote"
			editGroup: the name of the edit group as defined in the result object
		
		Returns:
			A list of documents including the document ID and additional data for display
		"""
		try:
			self.setParams({"researcherId": researcherId})
			if dataType == "metadata":				
				processPath = 'processors/queryMetadata.process.json'
			elif dataType == "project":
				self.setParams({"researcherId": researcherId, "allowedData": ["editedData", "transcription", "note", "extraction"], "allowedPermission": ["RomRepOnly", "open"]})
				processPath = "processors/queryProject.process.json"
			elif dataType == "consultant":
				processPath = "processors/queryAllConsultants.process.json"
			elif dataType == "commonNote":
				processPath = "processors/queryCommonNote.process.json"
			else:
				return "404 Bad data type"
			result = self.getDocumentListFromProcess(processPath, editGroup)
			return result
		except Exception as e:
			return e
	
	def linkDocuments(self, researcherId, dataType, editGroup, parentId, linkedId):
		"""Create an edge between two documents
		
		Checks the edit group definition and adds a link between two documents given if no such edge already exists and linking is permitted for this relation.
		
		Args:
			researcherId: the ID of the researcher currently signed in
			dataType: "metadata", "project", "consultant", "commonNote"
			editGroup: the name of the edit group as defined in the result object
			parentId: the ID of the parent or main document
			linkedId: the ID of the document to be linked to the parent or main document
		
		Returns:
			True if successful, or an error message otherwise
		"""
		try:
			self.setParams({"researcherId": researcherId})
			if dataType == "metadata":
				processPath = 'processors/queryMetadata.process.json'
			elif dataType == "project":
				self.setParams({"researcherId": researcherId, "allowedData": ["editedData", "transcription", "note", "extraction"], "allowedPermission": ["RomRepOnly", "open"]})
				processPath = "processors/queryProject.process.json"
			elif dataType == "consultant":
				processPath = "processors/queryAllConsultants.process.json"
			elif dataType == "commonNote":
				processPath = "processors/queryCommonNote.process.json"
			else:
				return "404 Bad data type"
			self.addEdge(processPath, editGroup, parentId, linkedId)
			return True
		except Exception as e:
			return e
	
	def unlinkDocuments(self, researcherId, dataType, editGroup, edgeId):
		"""Remove an edge between two documents
		
		Checks the edit group definition and removes the link between two documents given if linking is permitted for this relation.
		
		Args:
			researcherId: the ID of the researcher currently signed in
			dataType: "metadata", "project", "consultant", "commonNote"
			editGroup: the name of the edit group as defined in the result object
			edgeId: the ID of the edge to remove
		
		Returns:
			True if successful, or an error message otherwise		
		"""
		try:
			self.setParams({"researcherId": researcherId})
			if dataType == "metadata":				
				processPath = 'processors/queryMetadata.process.json'
			elif dataType == "project":
				processPath = "processors/queryProject.process.json"
			elif dataType == "consultant":
				processPath = "processors/queryAllConsultants.process.json"
			elif dataType == "commonNote":
				processPath = "processors/queryCommonNote.process.json"
			else:
				return "404 Bad data type"
			self.removeEdge(processPath, editGroup, edgeId)
			return True
		except Exception as e:
			return e

	def addNewCommonNote(self, researcherId, projectId, inputJSON):
		"""Add new common note
		
		Add a new common note JSON object to the database.
		
		Args:
			researcherId: the ID of the researcher currently signed in
			projectId: the ID of the project document the common note belongs to
			inputJSON: the JSON object created according to the input schema			
		Returns:
			True if successful and an error message otherwise
		"""
		try:
			self.setParams({"researcherId": researcherId, "projectId": projectId})
			self.processData(inputJSON, 'processors/setCommonNote.process.json', 'setCommonNote.schema.json', ['commonNote.schema.json'], "RomRepDataStorage")
			return True
		except Exception as e:
			return e
	
	def queryCommonNotes(self, researcherId, projectId):
		"""Get a list of common notes
		
		Retrieve a list of common notes from the database that match the query. 
		
		Args:
			researcherId: the ID of the researcher currently signed in
			projectId: the ID of the project document the common note belongs to
		
		Returns:
			A result object holding the document IDs found and additional information for displaying, and an error message otherwise
		"""
		try:
			self.setParams({"researcherId": researcherId, "projectId": projectId})
			result = self.processData({}, 'processors/queryAllCommonNotes.process.json', 'empty.schema.json', ['commonNote.schema.json'], "RomRepDataStorage")
			return result
		except Exception as e:
			return e	
	
