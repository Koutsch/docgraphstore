#!/usr/bin/python3

"""
Copyright 2021 Jakob Wiedner

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

from DBbasic import *
from pyArango.collection import *
from pyArango.graph import *
import json

class RRDScreator(RRDSbasic):
		
	# remove a collection
	def removeCollection(self, name):
		# check if collection already exists
		if self.DB.hasCollection(name):
			# delete if so
			oldCollection = self.DB[name]
			oldCollection.delete()
			self.DB.reloadCollections()		
	
	# create collections, graph and edge definitions
	def setupDB(self, config_path):
		# get structure
		with open(config_path) as json_file:
			print("OPENED STRUCTURE CONFIGURATION from file", config_path)
			structure = json.load(json_file)
			
			# create collections
			collections = structure["collections"]
			for c in collections:
				self.removeCollection(c)
				# create collection class
				type(c, (Collection, ), {})
				# create collection
				self.DB.createCollection(className=c)
				print("Collection", c, "set")
			
			# create edges
			edges = structure["edges"]
			edge_definitions = []
			for e in edges:
				# add to edge definition
				edge_definitions.append(EdgeDefinition(e["name"], fromCollections=[e["from"]], toCollections=[e["to"]]))
				self.removeCollection(e["name"])
				# create edge class
				type(e["name"], (Edges, ), {})
				# create edges
				self.DB.createCollection(className=e["name"])
				print("Edge collection", e["name"], "set")
				
			# create graph
			if self.DB.hasGraph(structure["name"]):
				# delete if exists
				graph = self.DB.graphs[structure["name"]]
				graph.delete()
				self.DB.reloadGraphs()
			# create graph class
			edge_definition_tuple = tuple(edge_definitions)
			type(structure["name"], (Graph, ), {
				"_edgeDefinitions": edge_definition_tuple,
				"_orphanedCollections": []
			})
			self.DB.createGraph(name=structure["name"], createCollections=False)
			print("Graph", structure["name"], "set")

rc = RRDScreator("root", "7u3WWqOlpp?m.7-F")
DB = rc.connect('romrep')
print(DB)
rc.setupDB("RRDS.structure.json")
