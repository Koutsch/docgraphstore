#!/usr/bin/python3

"""
Copyright 2021 Jakob Wiedner

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

from DBbasic import *
import json
import jsonschema
import copy
import os
import re
from jsonpath_ng.ext import *
import subprocess

class MissingKeyError(Exception):
	pass

class MissingSchemaError(Exception):
	pass

class PropertyError(Exception):
	pass

class InvalidDataError(Exception):
	pass

class IdError(Exception):
	pass

class EdgeExistsError(Exception):
	pass

class DBManipulationError(Exception):
	pass

class NotPermittedError(Exception):
	pass

class DBCreateError(Exception):
	pass

class RRDShandler(RRDSbasic):
	"""Extension of RRDSbasic for handling data manipulation
	
	Used for handling of user input, input validation and database querying.
	"""
	# dictionary holding schemas loaded as key-value pairs
	schemaStore = None
	# dictionary holding parameters loaded
	params = None
	# the initial Arango DB batch size
	batchSize = 100
	# get the directory separator used on the local machine
	dirSep = os.sep
	# set schema directory to current directory
	schemaDir = os.getcwd()
	# indicated whether files should be removed during validation
	removeFile = False
	
	def setSchemaPath(self, schemaDirPath):
		"""Set the absolute path to the folder containing the schemas
		
		The RRDSAPI demands that the ID of a schema equals its file name and that all schema files are in the same directory.
		Use this function for specifying the location of the schemas before loading a schema. If no folder is specified, the current working directory is used.
		
		Args:
			schemaDirPath: the path to the folder holding all schemas
		"""
		self.schemaDir = schemaDirPath
	
	def loadSchemasFromFile(self, schemaPaths):
		"""Load a schema to be used in validation
		
		The schema loaded is stored in the object and can be used to validate an arbitrary amount of JSON data.
		
		Args:
			schemaPaths: a list holding the paths to further schemas referenced by the base schema
		"""
		# check if schema store is set
		if self.schemaStore is None:
			self.schemaStore = {}
		# add schemas if not exist
		for schemaPath in schemaPaths:
			if schemaPath not in self.schemaStore:
				schema = json.loads(open(self.schemaDir + self.dirSep + schemaPath).read())
				self.schemaStore[schema.get('$id', schemaPath)] = schema
	
	def setParams(self, params):
		"""Set the parameters needed for data handling
		
		Some data handling and validations might rely on an external parameter, e.g. in case of database query validations.
		If such parameter is missing, an error is thrown during processing.
		
		Args:
			params: a dictionary holding the parameter name as key and the parameter value as value
		"""
		self.params = params

	def addEdge(self, processPath, group, mainId, linkedId):
		"""Add an edge between two documents
		
		Adds an edge between documents according to the process group definition.
		
		Args:
			processPath: the path to the process file
			group: the corresponding group name in the query processor
			mainId: the id of the parent document
			linkedId: the id of the linked subordinated document
		
		Raises:
			NotPermittedError: if linking is not permitted
			EdgeExistsError: if such an edge already exists
			AQLQueryError: if there is an error during data update
		"""
		# load process
		processor = json.loads(open(processPath).read())
		# get process group description
		processGroup = self.__getProcessGroup(processor, group)
		# get graph name
		graph = processor["query"]["structure"]["graph"]
		# check if editable
		if "editEdge" in processGroup and processGroup["editEdge"] == False:
			raise NotPermittedError("Adding this edge is not permitted")
		if "addEdgeCondition" in processGroup and False == self.__checkCondition(processGroup["addEdgeCondition"], mainId):
			raise NotPermittedError("Edge adding condition is not met")
		# check if new ID points to allowed
		self.__checkCorrectDocument(graph, processor["query"]["documentQueries"][group], processor["query"]["fields"], linkedId)		
		# check if already exists
		query = "LET doc = DOCUMENT('%s') FOR v, e IN ANY doc GRAPH '%s' FILTER IS_SAME_COLLECTION(e, '%s') && v._id == '%s' RETURN e" % (mainId, graph, processGroup["edge"], linkedId)
		result = self.DB.AQLQuery(query, rawResults=True, batchSize=self.batchSize)
		# raise error if result
		if len(result) > 0:
			raise EdgeExistsError("The two documents are already linked by such an edge")
		# get graph from DB
		graphObj = self.DB.graphs[graph]
		# check from and to collections
		ids = self.__checkEdgeCollection(graphObj, processGroup["edge"], mainId, linkedId, processGroup.get("direction"))
		# create edge
		graphObj.createEdge(processGroup["edge"], ids["from"], ids["to"], {})
	
	def removeEdge(self, processPath, group, id):
		"""Remove an edge by id
		
		Removes an edge by id after checking if the edge is editable.
		
		Args:
			processPath: the path to the process file
			group: the corresponding group name in the query processor
			id: the Arango DB id of the edge document to remove
		
		Raises:
			AQLQueryError: if there is an error during data update
			NotPermittedError: if the edge is not allowed to be edited
		"""
		# load processor
		processor = json.loads(open(processPath).read())
		# get processor group
		processGroup = self.__getProcessGroup(processor, group)	
		# check if editable
		if "editEdge" in processGroup and processGroup["editEdge"] == False:
			raise NotPermittedError("Removing this edge is not permitted")
		if "editEdgeCondition" in processGroup and False == self.__checkCondition(processGroup["editEdgeCondition"], id):
			raise NotPermittedError("Edge removal condition is not met")
		# get id parts
		idDict = self.__splitId(id)	
		# remove edge
		self.DB[idDict["collection"]][idDict["key"]].delete()	
	
	def removeDocument(self, processPath, group, id):
		"""Remove a document by id
		
		Removes a document by id after checking if editing is permitted.
		
		Args:
			processPath: the path to the process file
			group: the corresponding group name in the query processor
			id: the Arango DB id of the document to remove
		
		Raises:
			AQLQueryError: if there is an error during data update
			NotPermittedError: if the value is not allowed to be edited
		"""
		# load process group
		processor = json.loads(open(processPath).read())
		# get process group description
		processGroup = self.__getProcessGroup(processor, group)
		# check if editable
		if "edit" in processGroup and processGroup["edit"] == False:
			raise NotPermittedError("Removing this document is not permitted")
		if "editCondition" in processGroup and False == self.__checkCondition(processGroup["editCondition"], id):
			raise NotPermittedError("Document removal condition is not met")		
		# create query
		query = self.__createDocumentRemovalQuery(processor, processGroup, id)
		# get documents to remove		
		toRemove = self.DB.AQLQuery(query, rawResults=True, batchSize=self.batchSize)
		# check if exists
		if len(toRemove) < 1:
			raise IdError("Document '%s' does not exist" % (id))
		# set file removal variable true (causes file removal upon validation)
		self.removeFile = True
		try:
			# remove documents
			for document in toRemove[0]["documents"]:
				docDict = self.__splitId(document["id"])
				self.DB[docDict["collection"]][docDict["key"]].delete()
				# check for files to delete
				self.validate(document["document"], docDict["collection"] + ".schema.json")
			# remove edges
			for edge in toRemove[0]["edges"]:
				edgeDict = self.__splitId(edge["_id"])
				self.DB[edgeDict["collection"]][edgeDict["key"]].delete()
		except Exception as e:
			# on error, rebuild with backups if removed
			for document in toRemove[0]["documents"]:
				docDict = self.__splitId(document["id"])
				document["document"]["_key"] = docDict["key"]
				query = "UPSERT {_key: '%s'} INSERT %s UPDATE {} IN %s" % (docDict["key"], json.dumps(document["document"]), docDict["collection"])
				self.DB.AQLQuery(query, rawResults=True, batchSize=self.batchSize)
			for edge in toRemove[0]["edges"]:
				edgeDict = self.__splitId(edge["_id"])
				query = "UPSERT {_key: '%s'} INSERT %s UPDATE {} IN %s" % (edgeDict["key"], json.dumps(edge), edgeDict["collection"])
				self.DB.AQLQuery(query, rawResults=True, batchSize=self.batchSize)
			# reraise exception
			raise DBManipulationError(e)
		# set file remove to false again
		self.removeFile = False
	
	def getDocumentListFromSchema(self, schemaPath, path):
		"""Get a list of documents as defined in an input definition schema
		
		Queries a list of documents including the document id and additional fields as defined in a key-list type of the input definition schema.
		
		Args:
			processPath: the path to the processor file
			group: the name of the process group to use
		
		Returns:
			a list of dictionaries holding the document id (property "id") and the additional data (property "data")
		
		Raises:
			PropertyError: if path points to a non-existing definition or if the definition is not of type 'key-list'
			AQLQueryError: if there is an error during the data query
		"""
		# load schema if not exists
		self.loadSchemasFromFile([schemaPath + '.schema.json'])
		# get schema fragment according to path
		fragment = self.__getSchemaFragment(schemaPath, path)
		# check if it's key-list type
		if "type" not in fragment or fragment["type"] != "key-list" or "graph" not in fragment or "query" not in fragment or "fields" not in fragment:
			raise PropertyError("Not a key-list defintion")
		# get documents	
		result = self.__getDocuments(fragment["graph"], fragment["query"], fragment["fields"], None, True)
		# tranform result to list
		documents = self.__resultToList(result)
		return documents		
	
	def getDocumentListFromProcess(self, processPath, group):
		"""Get a list of documents as defined in a process group
		
		Queries a list of documents including the document id and additional fields as defined in the process group.
		
		Args:
			processPath: the path to the processor file
			group: the name of the process group to use
		
		Returns:
			a list of dictionaries holding the document id (property "id") and the additional data (property "data")
		
		Raises:
			AQLQueryError: if there is an error during the data query
		"""
		# load process group
		processor = json.loads(open(processPath).read())
		# get documents	
		result = self.__getDocuments(processor["query"]["structure"]["graph"], processor["query"]["documentQueries"][group], processor["query"]["fields"], None, True)
		# tranform result to list
		documents = self.__resultToList(result)
		return documents
	
	def validate(self, json, baseSchemaPath):
		"""Validate JSON data against schema
		
		The object given is validated against the JSON schema loaded via loadSchema.
		On successful validation, no error is thrown.
		
		A key-list property is compared against a document query as defined in the process schema, including the graph name and a field list.
		
		Args:
			json: a Python object representing the JSON data
			baseSchemaPath: the name of the basic JSON schema from which to begin the validation process
		
		Raises:
			MissingKeyError: if a parameter needed for validation is not given
			ValidationError: if the JSON given is invalid
			AQLQueryError: if the query of a query validator contains a syntax error
		"""
		# check if schemas loaded
		if self.schemaStore is None:
			raise MissingSchemaError("No schemas loaded yet")
		# clean JSON from empty data
		json = self.__cleanEmpty(json)
		# create a basic validator
		baseValidator = self.__createBasicValidator()
		# define custom type checks (key-list: for query validators, string: extended not to validate empty strings, file-path: for paths to data files, long-text: for long texts)
		type_checks = baseValidator.TYPE_CHECKER.redefine_many({
			'key-list': self.__isNonEmptyString,
			'string': self.__isNonEmptyString,
			'long-text': self.__isNonEmptyString,
			'file-path': self.__isFilePath
		})
		# add custom type checks
		validator = jsonschema.validators.extend(baseValidator, type_checker=type_checks)
		# create format checker
		format_checker = jsonschema.FormatChecker()
		# create referenced schema resolver
		resolver = jsonschema.RefResolver.from_schema(self.schemaStore[baseSchemaPath], store=self.schemaStore)
		# validate
		validator(schema=self.schemaStore[baseSchemaPath], resolver=resolver, format_checker=format_checker).validate(json)	
	
	def processData(self, inputJSON, processPath, inputSchemaPath, dependencyPaths, graphName = None):
		"""Process JSON data
		
		The input JSON is processed by a process description JSON and validated against schema.
		
		Args:
			inputJSON: a Python object representing the JSON data
			processPath: the path to the process description JSON
			inputSchemaPath: the schema for validating the inputJSON
			dependencyPaths: a list of paths to all additional schemas needed
			graphName: the name of the graph (default is None, meaning that no graph query is carried out)
		
		Raises:
			MissingKeyError: if a parameter needed for validation is not given
			ValidationError: if the JSON given is invalid
			AQLQueryError: if the query of a query validator contains a syntax error
			DBCreateError: if data storing is not successful
			PropertyError: if a property path points to non-existing values
		"""
		# load all necessary schemas
		self.__loadSchemas(inputSchemaPath, dependencyPaths)
		# validate input JSON
		self.validate(inputJSON, inputSchemaPath)
		# load processing description
		processor = json.loads(open(processPath).read())		
		# process vertices
		if "vertices" in processor:
			self.__processDataAdding(processor, inputJSON, graphName)							
		# process query
		elif "query" in processor:
			return self.__processQuery(processor["query"], inputJSON)

	def addToList(self, processPath, group, id, path, value):
		"""Add an item to document array property
		
		The value is checked if it already exists in the array and added to the end if not.
		
		Args:
			processPath: the path to the process file
			group: the corresponding group name in the query processor
			id: the Arango DB id of the document to edit
			path: the path to the property in JSON path-like form (PROPNAME.PROPNAME)
			value: the value to be added
		
		Raises:
			AQLQueryError: if there is an error during data update
			ValidationError: if the JSON given has become invalid after data had been edited
			NotEditableError: if the value is not allowed to be edited
		"""
		self.__updateDocument(processPath, group, id, path, value, 'a')
	
	def removeFromList(self, processPath, group, id, path, value):
		"""Remove an item to document array property
		
		The value is checked if it exists in the array and removed if yes.
		
		Args:
			processPath: the path to the process file
			group: the corresponding group name in the query processor
			id: the Arango DB id of the document to edit
			path: the path to the property in JSON path-like form (PROPNAME.PROPNAME)
			value: the value to be removed
		
		Raises:
			AQLQueryError: if there is an error during data update
			ValidationError: if the JSON given has become invalid after data had been edited
			NotEditableError: if the value is not allowed to be edited
		"""
		self.__updateDocument(processPath, group, id, path, value, 'r')
	
	def editValue(self, processPath, group, id, path, value, defaultPath=None, checksum=None):
		"""Edit document property
		
		Edit the value of a single property in a document.
		
		Args:
			processPath: the path to the process file
			group: the corresponding group name in the query processor
			id: the Arango DB id of the document to edit
			path: the path to the property in JSON path-like form (PROPNAME.PROPNAME[INDEX])
			value: the value to be set instead of the old value
			defaultPath: in case of values the path of which contains an array index the default path as defined in the process JSON must be given
			checksum: in case of a long-text type the checksum from the result object must be given for intermediate change control
		
		Raises:
			AQLQueryError: if there is an error during data update
			ValidationError: if the JSON given has become invalid after data had been edited
			NotEditableError: if the value is not allowed to be edited
		"""
		self.__updateDocument(processPath, group, id, path, value, 'e', defaultPath, checksum)

	def __createDocumentRemovalQuery(self, processor, processGroup, id):
		# check unremovables
		unremovables = ""
		if "unremovable" in processor["query"] and len(processor["query"]["unremovable"]):
			unremovables = "FILTER !IS_SAME_COLLECTION(v, '" + "') && !IS_SAME_COLLECTION('".join(processor["query"]["unremovable"]) + "')"
		if "unremovable" in processGroup and len(processGroup["unremovable"]):
			unremovables = "FILTER !IS_SAME_COLLECTION(v, '" + "') && !IS_SAME_COLLECTION('".join(processGroup["unremovable"]) + "')"
		# get related documents with no other edges
		graph = processor["query"]["structure"]["graph"]
		# assemble query and add variables
		query = """LET doc = DOCUMENT('%s')
FILTER LENGTH(doc) > 0
LET single = (
    FOR v, e IN 1..1 ANY doc GRAPH '%s'
    %s
    LET ec = LENGTH(FOR vl, el IN 1..1 ANY v GRAPH '%s' RETURN 1) 
    FILTER ec < 2 
    RETURN {"id": v._id, "document": UNSET(v, '_id', '_key', '_rev')}
)
LET d_edges = (
    FOR v, e IN 1..1 ANY doc GRAPH '%s' 
    RETURN e
)
RETURN {
		"documents": UNION([{"id": doc._id, "document": UNSET(doc, '_id', '_key', '_rev')}], single),
    "edges": d_edges
}
""" % (id, graph, unremovables, graph, graph)
		return query
	
	def __isFilePath(self, checker, inst): # HIER WEITER schön machen
		# TODO: check file upload, depends on implementation
		
		# remove file if option is set true
		print("Remove file?", self.removeFile, inst)
		
		return isinstance(inst, str)
	
	def __isNonEmptyString(self, checker, inst):
		# check if string and at least length 1
		return isinstance(inst, str) and len(inst) > 0
	
	def __insertParams(self, query, params):
		# External params loaded via setParams can be inserted by using a {{PARAMNAME}} wildcard.
		# loop all parameters given
		for param in params:
			# check if parameter set
			if self.params is not None and param in self.params:
				query = query.replace("{{%s}}" % (param), "'%s'" % (self.params[param]))
			else:
				raise MissingKeyError("Key '%s' is missing" % (param))
		# return querywith insertions
		return query
	
	def __isQuery(self, validator, value, instance, schema):		
		# check if document really exists
		result = self.__getDocuments(schema["graph"], value, schema["fields"], instance)
		if len(result) == 0:
			yield jsonschema.ValidationError("'%s' has not been found in the data" % (instance))		
	
	def __cleanEmpty(self, d):
		# remove empty dictionaries
		if isinstance(d, dict):
			return {
				k: v 
				for k, v in ((k, self.__cleanEmpty(v)) for k, v in d.items())
				if v
			}
		# remove empty lists
		if isinstance(d, list):
			return [v for v in map(self.__cleanEmpty, d) if v]
		# return only non-empty strings
		if isinstance(d, str):
			if d:
				return d
		# return the rest
		else:
			return d
	
	def __createBasicValidator(self):
		# assemble validators used (standard draft7 and custom database query validator)
		validators = dict(jsonschema.Draft7Validator.VALIDATORS)
		validators['query'] = self.__isQuery
		# create a basic validator
		baseValidator = jsonschema.validators.create(
			meta_schema=jsonschema.Draft7Validator.META_SCHEMA,
			validators=validators
		)
		return baseValidator
	
	def __getPropertyDef(self, propertyDefStr):
		# split data field string
		parts = propertyDefStr.split(":")
		return {"type": parts[0], "path": parts[1]}
	
	def __queryJSON(self, query, JSONObj):
		# parse query
		jsonpathExp = parse(query)
		# make query
		result = jsonpathExp.find(JSONObj)
		# return result or None if empty
		if len(result) == 0:
			return None
		else:
			return result
	
	def __processField(self, path, inputJSON):
		# get from input JSON
		data = self.__queryJSON(path, inputJSON)
		# check if result
		if data is None:
			return None
		return data[0].value
	
	def __processFields(self, path, inputJSON):
		# get from input JSON
		data = self.__queryJSON(path, inputJSON)
		# check if result
		if data is None:
			return None
		# return values as list
		return [d.value for d in data]
	
	def __processFile(self, path, inputJSON):
		# TODO get file > das dann je nach file handling implementieren
		return "generic_filename;audio/ogg"		
	
	def __processParam(self, path):
		# return internal param
		if self.params is not None and path in self.params:
			return self.params[path]
		else:
			raise MissingKeyError("Key '%s' is missing" % (path))
	
	def __processVertex(self, path, vertexColl):
		# split path
		vertexPath = path.split(".")
		# get vertex definition
		vertexDef = vertexColl[vertexPath[0]]
		# return document definitions as list
		return [v[vertexPath[1]] for v in vertexDef]		
	
	def __processPropertyDef(self, propertyDefStr, inputJSON=None, vertexColl=None):
		# get data field description
		propertyDef = self.__getPropertyDef(propertyDefStr)		
		# process according to type
		# field
		if propertyDef["type"] == "field" and inputJSON is not None:
			return self.__processField(propertyDef["path"], inputJSON)
		# fields
		elif propertyDef["type"] == "fields" and inputJSON is not None:
			return self.__processFields(propertyDef["path"], inputJSON)
		# file
		elif propertyDef["type"] == "file":
			return self.__processFile(propertyDef["path"], inputJSON)
		# param
		elif propertyDef["type"] == "param":
			return self.__processParam(propertyDef["path"])
		# vertex
		if propertyDef["type"] == "vertex" and vertexColl is not None:
			return self.__processVertex(propertyDef["path"], vertexColl)
		# in all other cases return None
		else:
			return None
	
	def __fillTemplateJSON(self, docTemplateJSON, inputJSON, parent=None, parentKey=None):
		# go to sub if dictionary
		if isinstance(docTemplateJSON, dict):
			for key in docTemplateJSON:
				self.__fillTemplateJSON(docTemplateJSON[key], inputJSON, docTemplateJSON, key)
		# go to sub if list
		elif isinstance(docTemplateJSON, list):
			for i in range(0, len(docTemplateJSON)):
				self.__fillTemplateJSON(docTemplateJSON[i], inputJSON, docTemplateJSON, i)
		# process property definition if string
		elif isinstance(docTemplateJSON, str) and parent is not None:
			# get value from property definition
			value = self.__processPropertyDef(docTemplateJSON, inputJSON)
			# add to parent
			parent[parentKey] = value
	
	def __createDocumentDef(self, docDef, inputValue, collectionName):
		# get document template
		documentObj = copy.deepcopy(docDef["document"])
		collectionName = docDef["collection"]
		self.__fillTemplateJSON(documentObj, inputValue)
		# validate
		self.validate(documentObj, "%s.schema.json" % (collectionName))
		# return document json object
		return documentObj
	
	def __processVertexDef(self, vertexDef, inputJSON):
		# prepare list for storing the documents to be created
		vertexList = []
		# get json root
		rootLevelInputs = self.__queryJSON(vertexDef["root"], inputJSON)
		# check if there are root level results
		if rootLevelInputs is None:
			return vertexList
		# get documents definition
		documents = vertexDef["documents"]
		# loop all root level results
		for rootInput in rootLevelInputs:
			# prepare document collection dict
			docColl = {}
			# append to vertex list
			vertexList.append(docColl)
			# loop documents
			for docDef in documents:
				# assign to document collection
				collectionName = documents[docDef]["collection"]
				docColl[docDef] = {"collection": collectionName, "document": self.__createDocumentDef(documents[docDef], rootInput.value, collectionName)} 
		# return vertex list
		return vertexList
		
	def __processVertices(self, vertices, inputJSON):
		# prepare vertex collection
		vertexColl = {}
		# loop vertex definitions
		for key in vertices:
			vertexColl[key] = self.__processVertexDef(vertices[key], inputJSON)
		# return filled vertex collection
		return vertexColl
	
	def __getFromToValues(self, edgeDef, inputJSON, vertexColl):
		# get from value
		fromValue = self.__processPropertyDef(edgeDef["from"], inputJSON=inputJSON, vertexColl=vertexColl)		
		# make list of string
		if not isinstance(fromValue, list):
			fromValue = [fromValue]
		# get to value
		toValue = self.__processPropertyDef(edgeDef["to"], inputJSON=inputJSON, vertexColl=vertexColl)
		if not isinstance(toValue, list):
			toValue = [toValue]
		# return as dictionary
		return {"from": fromValue, "to": toValue}
	
	def __createEdgeInDB(self, graph, collectionName, fromValue, toValue, idList):
		try:
			# create edge in DB
			edge = graph.createEdge(collectionName, fromValue, toValue, {}, False)
			# add to id list
			idList.append(edge._id)
			return True
		except Exception as e:
			# return error message
			return e
	
	def __createEdges(self, values, edgeDef, graph):
		# prepare id list of documents created
		idList = []
		success = True
		# check if same vertices definitions
		if (edgeDef["from"].split('.')[0] == edgeDef["to"].split('.')[0]):
			# create by index
			for i in range(0, len(values["from"])):
				result = self.__createEdgeInDB(graph, edgeDef["collection"], values["from"][i], values["to"][i], idList)
				if result is not True:
					return {"ids": idList, "success": result}
		else:
			# create edges between all from and to documents
			for f in values["from"]:
				for t in values["to"]:
					# check if exist
					if f is not None and t is not None:
						result = self.__createEdgeInDB(graph, edgeDef["collection"], f, t, idList)
						if result is not True:
							return {"ids": idList, "success": result}
		# return success state (True of error message) and id list
		return {"ids": idList, "success": success}
				
	def __processEdges(self, edgesDef, inputJSON, vertexColl, graphName):
		# prepare id list of documents created
		idList = []
		# check graph exists
		if graphName is None:
			return {"success": "Edges cannot be created since name of graph is missing", "ids": idList}
		# get graph
		graph = self.DB.graphs[graphName]
		# loop edge definitions
		for key in edgesDef:
			# get from and to values
			values = self.__getFromToValues(edgesDef[key], inputJSON, vertexColl)
			result = self.__createEdges(values, edgesDef[key], graph)
			idList += result["ids"]
			# interrupt if error
			if result["success"] is not True:
				return {"success": result["success"], "ids": idList}
		# if went through return success message
		return {"success": True, "ids": idList}
	
	# TODO other operators
	def __createFilterExpression(self, fields, filterDef, collDef, vertexVar, value):
		isArray = False
		expression = ""
		# if field, get type and check if array
		if fields[filterDef["field"]].startswith("field"):
			fragment = self.__getSchemaFragment(collDef["collection"], filterDef["property"])
			if fragment["type"] == "array":
				isArray = True
		# create accordingly
		if isArray:
			expression = "POSITION(c_%s, '%s')" % ("%s.%s" % (vertexVar, filterDef["property"]), value)
		else:
			expression = "c_%s == '%s'" % ("%s.%s" % (vertexVar, filterDef["property"]), value)
		# add operator
		if "operator" in filterDef:
			# not
			if filterDef["operator"] == "not":
				expression = "!(%s)" % (expression)
		# return full expression
		return expression
	
	def __createPropertyFilter(self, filterList, fields, filterDef, collDef, vertexVar, inputJSON):
		# get property value
		propertyValue = self.__processPropertyDef(fields[filterDef["field"]], inputJSON)
		# check if value exists
		if propertyValue is None:
			return
		# create filter expression according to type (string or list)
		if isinstance(propertyValue, list):
			# prepare value filter list
			valueFilterList = []
			for value in propertyValue:
				valueFilterList.append(self.__createFilterExpression(fields, filterDef, collDef, vertexVar, value))
			# join value filter list with or and add to filter list
			filterList.append("(%s)" % (" || ".join(valueFilterList)))
		elif isinstance(propertyValue, str):
			filterList.append(self.__createFilterExpression(fields, filterDef, collDef, vertexVar, propertyValue))
		else:
			raise InvalidDataError("Input data can only be strings or lists")
	
	def __createFilters(self, collDef, vertexVar, fields, inputJSON, add=False):
		# prepare query
		query = ""
		# check if filters exist
		if "filters" not in collDef or len(collDef["filters"]) == 0:
			return query
		# prepare filter list
		filterList = []
		# loop filter definitions
		for filterDef in collDef["filters"]:
			# add filter expressions to list
			self.__createPropertyFilter(filterList, fields, filterDef, collDef, vertexVar, inputJSON)
		# return empty if no filters
		if len(filterList) == 0:
			return query
		# add prefix
		if add:
			query += " && "
		else:
			query += " FILTER "
		# join filter list to expression
		query += " && ".join(filterList)
		# return query
		return query
	
	def __createDirectQuery(self, queryDef, vertexVar, fields, inputJSON):
		query = "FOR c_%s IN %s" % (vertexVar, queryDef["collection"])
		# create filters
		query += self.__createFilters(queryDef, vertexVar, fields, inputJSON)
		return query		
	
	def __createEdgeQuery(self, collDef, vertexVar, prevvertexVar, graphName, fields, inputJSON):
		# prepare collection filter
		collFilter = ""
		# check if single or multiple edges
		if isinstance(collDef["edge"], str):
			collFilter = "IS_SAME_COLLECTION('%s', e_%s)" % (collDef["edge"], vertexVar)
		elif isinstance(collDef["edge"], list):
			filterList = []
			for edge in collDef["edge"]:
				filterList.append("IS_SAME_COLLECTION('%s', e_%s)" % (edge, vertexVar))
			collFilter = " || ".join(filterList)
		else:
			raise PropertyError("The edge definition is invalid (please review query processor)")
		query = " FOR c_%s, e_%s, p_%s IN 1..1 ANY c_%s GRAPH '%s' FILTER %s " % (vertexVar, vertexVar, vertexVar, prevvertexVar, graphName, collFilter)
		# create filters
		query += self.__createFilters(collDef, vertexVar, fields, inputJSON, True)
		return query
	
	# TODO sollte wenn nötig auch refs folgen können
	def __getSchemaFragment(self, collectionName, path):
		# create schema file name
		schemaFile = "%s.schema.json" % (collectionName)
		# check if schema is loaded
		if self.schemaStore is None or schemaFile not in self.schemaStore:
			raise MissingSchemaError("Schema '%s' holding field type not loaded" % (schemaFile))
		# prepare path
		schemaPath = "$"
		pathList = path.split(".")
		# assemble full schema path
		for segment in pathList:
			# replace array search
			segment = "'%s'" % (segment)
			segment = re.sub(r'\[[^[]*\]\'', "'.items", segment)
			schemaPath += "..properties.%s" % (segment)
		# get schema fragment
		jsonpathExp = parse(schemaPath)
		schemaFragment = jsonpathExp.find(self.schemaStore[schemaFile])
		# check if exists
		if len(schemaFragment) < 1:
			raise PropertyError("Path '%s' points to non-existing value" % (path))
		# return schema type
		return schemaFragment[0].value
	
	def __processArrayProperty(self, pathParts, vertexVar):
		# prepare qeury
		query = ""
		# get last part
		lastPart = pathParts.pop()
		# get length of parts
		partLength = len(pathParts)
		# set vertex as parent value
		parentVar = "c_%s" % (vertexVar)
		# prepare first
		first = True
		# prepare current variable
		curVar = "s"
		# loop parts
		for i in range(0, partLength):
			if first:
				parentVar = "%s.%s" % (parentVar, pathParts[i])
				first = False
			else:
				parentVar = "%s[%s].%s" % (parentVar, curVar, pathParts[i])
				curVar += "s"
			# add to query
			query += 	"FOR %s IN RANGE(0, LENGTH(%s) - 1) FILTER %s >= 0 " % (curVar, parentVar, curVar)
		# create full single property path
		fullPath = "%s[%s]%s" % (parentVar, curVar, ("." + lastPart if lastPart != "" else ""))
		# return entire array property return
		return {"query": query, "path": fullPath}

	# TODO: also other edit modes
	def __checkEditMode(self, editMode, path,  propType):
		# check difference
		if editMode == "diff" and propType == "long-text":
			# add checksum
			return "'checksum': CRC32(%s)" % ( path)
		return None
	
	def __createArrayPropertyReturn(self, queryDef, properties, pathParts, vertexVar, property, propType):
		# create the array property return
		arrPropReturn = self.__processArrayProperty(pathParts, vertexVar)
		# set value query
		valueQuery = arrPropReturn["query"]	
		# unite query
		valueQuery += "LET sv = %s FILTER sv != null" % (arrPropReturn["path"])
		# add value property to properties
		properties.append("'value': sv")
		# get property edit permission
		propertyEdit = ("true" if "edit" in property and property["edit"] == True else "false")
		# get actual path
		realPath = "%s" % (re.sub(r"\[([^\[]*)\]", "[', \\1, ']", arrPropReturn["path"].split(".", 1)[1]))
		# check edit mode
		editModeProps = None
		if "editMode" in property:
			editModeProps = self.__checkEditMode(property["editMode"], arrPropReturn["path"], propType)
		# create query part with or without edit
		propertyQuery = " ((ed_%s == true && %s) || (%s && %s) ? MERGE({%s}, {'path': %s, 'defaultPath': '%s'%s}) : {%s}) " % (
			vertexVar,
			propertyEdit,
			("true" if "documentOnlyCondition" in queryDef and queryDef["documentOnlyCondition"] == True else "false"),
			propertyEdit,
			", ".join(properties),
			"CONCAT('%s')" % (realPath),
			property["path"],
			(", " + editModeProps if editModeProps != None else ""),
			", ".join(properties)
		)			
		# assemble subQuery
		return "{'title': '%s', 'values': (%s RETURN %s)}" % (property["title"], valueQuery, propertyQuery)
	
	# TODO: in the current design an array can only have one items type
	def __createPropertyReturn(self, vertexVar, queryDef):
		# check if property definitions in query definition
		if "properties" not in queryDef:
			return ""
		# prepare property list
		propertyList = []
		# loop properties
		for property in queryDef["properties"]:
			# get schema fragment
			fragment = self.__getSchemaFragment(queryDef["collection"], property["path"])
			# get property type
			propType = fragment["type"]
			# create return properties
			properties = [
				"'type': '%s'" % (propType)
			]
			# split path between array definitions
			pathParts = re.split(r'(?:\[\*\])\.?', property["path"])
			# check if contains arrays
			if len(pathParts) > 1:
				# create array subquery
				subQuery = self.__createArrayPropertyReturn(queryDef, properties, pathParts, vertexVar, property, propType)
				# add subquery to list
				propertyList.append(subQuery)
			# if no array given
			else:
				# add value property to properties
				properties.append("'value': c_%s.%s" % (vertexVar, property["path"]))
				# add title
				properties.append("'title': '%s'" % (property["title"]))
				# add properties to list
				propertyEdit = ("true" if "edit" in property and property["edit"] == True else "false")
				# check edit mode
				editModeProps = None
				if "editMode" in property:
					editModeProps = self.__checkEditMode(property["editMode"], "c_%s.%s" % (vertexVar,  property["path"]), propType)
				# assemble query
				propertyQuery = " ((ed_%s == true && %s) || (%s && %s) ? MERGE({%s}, {'id': c_%s._id, 'group': '%s'%s%s}) : {%s}) " % (
					vertexVar, 
					propertyEdit,
					("true" if "documentOnlyCondition" in queryDef and queryDef["documentOnlyCondition"] == True else "false"),
					propertyEdit,
					", ".join(properties), 
					vertexVar, 
					queryDef["name"], 
					", 'path': '%s'" % (property["path"]) if len(pathParts) == 1 else "",
					(", " + editModeProps if editModeProps != None else ""),
					", ".join(properties)
				)				
				# subquery to list
				propertyList.append(propertyQuery)								
		# join and return return expression
		return ", ".join(propertyList)
		
	def __createEntryPoints(self, entryPoints, graph, fields, inputJSON):
		# prepare query and current vertex variable
		query = ""
		curVertexVar = "ep"
		# get initial entry point
		initialEntryPoint = entryPoints.pop(0)
		# create initial entry point query
		query += self.__createDirectQuery(initialEntryPoint, curVertexVar, fields, inputJSON)
		# loop remaining entry points
		for i in range(0, len(entryPoints)):
			subVertexVar = curVertexVar + str(i)
			query += self.__createEdgeQuery(entryPoints[i], subVertexVar, curVertexVar, graph, fields, inputJSON)
			curVertexVar = subVertexVar
		# return query fragment and previous entry point variable name
		return {"var": curVertexVar, "query": query}
	
	def __checkConditions(self, conditions):
		# loop conditions
		for condition in conditions:
			result = self.__checkCondition(condition)
			# if no result return false
			if result == False:
				return False
		# otherwise true
		return True
	
	def __checkCondition(self, condition, id=None):
		query = self.__insertParams(condition["query"], condition["params"])
		# the current edge is added via the {{{value}}} wildcard
		if id is not None:
			query = query.replace("{{{value}}}", "'%s'" % (id))
		queryResult = self.DB.AQLQuery(query, rawResults=True, batchSize=self.batchSize)
		# if no result return false
		if len(queryResult) == 0:
			return False
		# otherwise true
		return True			
 
	def __createConditions(self, conditions, vertexVar, graph, fields, inputJSON):
		# prepare query string and condition variable
		query = ""
		condVar = "cd"
		# loop conditions
		for i in range(0, len(conditions)):
			# add expression
			query += " LET %s = (%s RETURN c_%s) FILTER LENGTH(%s) > 0 " % (condVar + str(i), self.__createEdgeQuery(conditions[i], condVar, vertexVar, graph, fields, inputJSON), condVar, condVar + str(i))
		# return all conditions
		return query
	
	def __createLinkedQueryExpression(self, linkedQueryDef, parentVertexVar, vertexVar, linkedQuery):
		# create full expression
		linkedQuery = " LET %s = (%s) " % (vertexVar, linkedQuery)
		# create add edge condition
		addEdgeCondition = "LET saed_%s = true" % (vertexVar)
		if "addEdgeCondition" in linkedQueryDef:
			addEdgeCondition = self.__createEditConditionQuery(linkedQueryDef["addEdgeCondition"], "saed_%s" % (vertexVar), "c_%s._id" % (parentVertexVar))
		# assemble query return part
		linkedReturn = "'%s': (%s LET ml_%s = {'title': '%s', 'documents': LENGTH(%s) ? %s : []} RETURN ((!%s ? false : saed_%s) ? MERGE(ml_%s, {'edgeAdd': {'parentDocument': c_%s._id, 'group': '%s'}}) : ml_%s)) " % (
			linkedQueryDef["name"],
			addEdgeCondition,
			vertexVar,
			linkedQueryDef["title"],
			vertexVar,
			vertexVar,
			"true" if "editEdge" in linkedQueryDef and linkedQueryDef["editEdge"] == True else "false",
			vertexVar,
			vertexVar,
			parentVertexVar,
			linkedQueryDef["name"],
			vertexVar
		)
		return {"query": linkedQuery, "return": linkedReturn}
		
	def __createQuery(self, queryDef, vertexVar, fields, inputJSON, graph, linkedDoc=None):
		query = ""
		# create clause part
		if linkedDoc is None:
			query += self.__createDirectQuery(queryDef, vertexVar, fields, inputJSON)
		elif "edge" in queryDef and linkedDoc is not None:
			query += self.__createEdgeQuery(queryDef, vertexVar, linkedDoc, graph, fields, inputJSON)
		else:
			raise Exception("Main query has invalid entry point (Review process definition)")
		# create conditions
		if "conditions" in queryDef:
			query += self.__createConditions(queryDef["conditions"], vertexVar, graph, fields, inputJSON)
		# prepare linked return expression list
		linkedReturns = []
		# check if linked
		if "linked" in queryDef:
			# loop linked
			for i in range(0, len(queryDef["linked"])):
				# check if condition
				condRes = True
				if "queryConditions" in queryDef["linked"][i]:
					condRes = self.__checkConditions(queryDef["linked"][i]["queryConditions"])
				# create if conditions ok
				if condRes:
					# get linked
					linked = self.__createLinkedQueryExpression(queryDef["linked"][i], vertexVar, vertexVar + str(i), self.__createQuery(queryDef["linked"][i], vertexVar + str(i), fields, inputJSON, graph, vertexVar))
					# add
					query += linked["query"]
					linkedReturns.append(linked["return"])
		# create return part
		returnProps = [
			"'values': [%s]" % (self.__createPropertyReturn(vertexVar, queryDef))
		]
		# add linked if exist
		if len(linkedReturns) > 0:
			returnProps.append("'linked': {%s}" % (", ".join(linkedReturns)))
		# check if edge editable	
		edgeEditCondition = "LET seed_%s = true" % (vertexVar)
		if "editEdgeCondition" in queryDef:
			edgeEditCondition = self.__createEditConditionQuery(queryDef["editEdgeCondition"], "seed_%s" % (vertexVar), "e_%s._id" % (vertexVar))
		documentQuery = edgeEditCondition
		edgeEditable = ("true" if "editEdge" in queryDef and queryDef["editEdge"] == True else "false")
		# check if edge editable
		editCondition = " LET sed_%s = true" % (vertexVar)
		if "editCondition" in queryDef:
			editCondition = self.__createEditConditionQuery(queryDef["editCondition"], "sed_%s" % (vertexVar), "c_%s._id" % (vertexVar))
		documentQuery += editCondition
		editable = ("true" if "edit" in queryDef and queryDef["edit"] == True else "false")
		# assemble query
		documentQuery += " LET ed_%s = (!%s ? false : sed_%s) LET eed_%s = (!%s ? false : seed_%s)" % (
			vertexVar,
			editable,
			vertexVar,
			vertexVar,
			edgeEditable,
			vertexVar
		)
		documentQuery += " LET pre = {'id': c_%s._id, %s} LET pre_e = (ed_%s == true ? MERGE({'documentEdit': {'id': c_%s._id, 'group': '%s'}}, pre) : pre) LET pre_ee = (eed_%s == true ? MERGE({'edgeEdit': {'id': e_%s._id, 'group': '%s'}}, pre_e) : pre_e) RETURN pre_ee" % (vertexVar, ", ".join(returnProps), vertexVar, vertexVar, queryDef["name"], vertexVar, vertexVar, queryDef["name"])
		# add to query
		query += documentQuery
		# return full query
		return query

	def __resultToList(self, result):
		documents = []
		for r in result:
			# append result to list
			documents.append(r)
		return documents
	
	def __createEditConditionQuery(self, condition, condName, valueCheckExpr=None):
		# insert parameters
		condQuery = self.__insertParams(condition["query"], condition["params"])
		if valueCheckExpr is not None:
			# replace {{{value}}} wildcard with value query expression
			condQuery = condQuery.replace("{{{value}}}", valueCheckExpr)
		# return full condition query
		return " LET q_%s = (%s) LET %s = (LENGTH(q_%s) > 0 ? true : false) " % (condName, condQuery, condName, condName)

	def __processDataAdding(self, processor, inputJSON, graphName):
		# check if conditions met
		if "conditions" in processor and False == self.__checkConditions(processor["conditions"]):
			raise NotPermittedError("Adding this data is not permitted")			
		# get document collection
		vertexColl = self.__processVertices(processor["vertices"], inputJSON)
		# create documents
		result = self.__createDocumentsInDB(vertexColl)
		# in case of error raise exception and remove all documents created
		if result["success"] is not True:
			self.__removeDocuments(result["ids"])
			raise DBCreateError(result["success"])
		# process edges
		if "edges" in processor:
			edgeResult = self.__processEdges(processor["edges"], inputJSON, vertexColl, graphName)
			# in case of error raise exception and remove all documents and edges created
			if edgeResult["success"] is not True:
				self.__removeDocuments(result["ids"])
				self.__removeDocuments(edgeResult["ids"])
				raise DBCreateError(edgeResult["success"])	
	
	# TODO: for now it just can access a single graph
	def __processQuery(self, queryDef, inputJSON):		
		# get structure and fields description
		structure = queryDef["structure"]
		fields = queryDef["fields"]		
		# prepare query and entry point vertex variable
		query = ""
		entryPointVar = None		
		# process entry points
		if "entries" in structure:
			entryPoints = self.__createEntryPoints(structure["entries"], structure["graph"], fields, inputJSON)
			query += entryPoints["query"]
			# set entry point variable name for main query
			entryPointVar = entryPoints["var"]
		# check if main query is given
		if "main" not in structure:
			raise Exception("Main query not defined")
		# check if multiple main
		if isinstance(structure["main"], list):
			# process main queries
			counter = 0
			returnVars = []
			for main in structure["main"]:
				returnVar = "m" + str(counter)
				returnVars.append(returnVar)
				query += "LET %s = (%s)" % ("m" + str(counter), self.__createQuery(main, returnVar, fields, inputJSON, structure["graph"], entryPointVar)) 
				counter += 1
			# add returns
			query += " RETURN [%s]" % (", ".join(returnVars))
		else:
			# process main query
			query += self.__createQuery(structure["main"], "m", fields, inputJSON, structure["graph"], entryPointVar)
		# execute query and return
		queryResult = self.DB.AQLQuery(query, rawResults=True, batchSize=self.batchSize)
		documents = self.__resultToList(queryResult)		
		# check if multiple main
		if isinstance(structure["main"], list):
			result = {}
			for i in range(0, len(structure["main"])):
				if len(documents[0][i]) > 0:
					# add main title and documents to result
					result[structure["main"][i]["name"]] = {
						'title': structure["main"][i]["title"],
						'documents': documents[0][i]
				}
		else:
			# add main title and documents to result
			result = {
				structure["main"]["name"]: {
					'title': structure["main"]["title"],
					'documents': documents
				}
			}
		# return the entire result
		return result
	
	def __loadSchemas(self, schemaPath, dependencyPaths):
		# set primary schema path
		schemaPaths = [schemaPath]
		# set dependency paths
		if dependencyPaths is not None:
			schemaPaths += dependencyPaths
		# load schemas
		self.loadSchemasFromFile(schemaPaths)		

	def __createDocumentsInDB(self, vertices):
		# prepare id list of documents created
		idList = []
		success = True
		# loop vertex definitions
		for vertexGroupDef in vertices:
			# loop document definitions
			for documents in vertices[vertexGroupDef]:				
				# loop documents
				for docDef in documents:
					# create in db
					try:
						collection = self.DB[documents[docDef]["collection"]]
						document = collection.createDocument(documents[docDef]["document"])
						document.save(True)
						documents[docDef] = document._id
						# append to successful id list
						idList.append(document._id)
					except Exception as e:
						success = e
		# return success state (True of error message) and id list
		return {"ids": idList, "success": success}
	
	def __removeDocuments(self, ids):
		# loop id list
		for id in ids:
			# split
			idDict = self.__splitId(id)
			# remove document
			self.DB[idDict["collection"]][idDict["key"]].delete()
	
	def __checkPropertyEditable(self, processor, group, path, id=None):
		# parse the JSON path expression
		jsonpathExp = parse("$.query..*[?(@.name='%s')]" % (group))
		processGroup = jsonpathExp.find(processor)
		if len(processGroup) != 1:
			raise InvalidDataError("No such definition")
		processGroup = processGroup[0].value
		# check if edit condition
		if (("documentOnlyCondition" not in processGroup or processGroup["documentOnlyCondition"] == False) and ("editCondition" in processGroup and False == self.__checkCondition(processGroup["editCondition"], id))):
			raise NotPermittedError("Not permitted")
		if "properties" not in processGroup:
			raise NotPermittedError("Property '%s' is not editable (Please review process description)" % (path))
		# check if edit is True
		for property in processGroup["properties"]:
			if property["path"] == path and property["edit"] == True:
				return property
		raise NotPermittedError("Property '%s' is not editable (Please review process description)" % (path))
	
	def __getProcessGroup(self, processor, group):
		# parse the JSON path expression
		jsonpathExp = parse("$.query..*[?(@.name='%s')]" % (group))
		# query processor
		data = jsonpathExp.find(processor)
		# return first occurrence or None otherwise
		if len(data) != 1:
			return None
		else:
			return data[0].value	
	
	def __splitId(self, id):
		# split id
		parts = id.split("/")
		# check if correct
		if len(parts) != 2:
			raise IdError("'%s' is not a valid Arango id" % (id))
		# return as dict
		return {"collection": parts[0], "key": parts[1]}
	
	def __getLastPathPart(self, path, parentValue):
		# split into parts (brackets and dots)
		splitPath = re.split(r'(?:[\[\]\.])', path)
		# check if last empty
		if splitPath[-1] == '':
			splitPath.pop()
		# return either integer or string
		if splitPath[-1].isnumeric() and isinstance(parentValue, list):
			return int(splitPath[-1])
		else:
			return splitPath[-1]
	
	# TODO: also other edit modes
	def __processEditMode(self, editModeDef, value, schemaType, data, lastPart, checksum=None):
		# process string diff mode
		if editModeDef == "diff" and schemaType == "long-text":
			# check if checksum given
			if checksum == None:
				raise PropertyError("No checksum given")
			# get old value
			oldValue = data.context.value[lastPart]
			# get checksum of old value
			oldChecksum = self.DB.AQLQuery("LET v = '%s' RETURN CRC32(v)" % (oldValue.replace('\'', '\\\'')), rawResults=True, batchSize=self.batchSize)[0]
			# if no changes since loading
			if checksum == oldChecksum:
				# simply replace the old value
				data.context.value[lastPart] = value
			# if data have been changed in the meantime
			else:
				# check diff via js				
				p = subprocess.Popen(['node', './quillDiff/quillDiff.js', oldValue, value], stdout=subprocess.PIPE)
				out = p.stdout.read()
				p.poll()
				# check if success
				if p.returncode != 0:
					raise InvalidDataError("The long text value is not valid:", out)
				# save diff
				data.context.value[lastPart] = out.decode("utf-8")
		# raise error if invalid edit mode definition
		else:
			raise PropertyError("The edit mode and the data type are not compatible (please review query definition and schema)")							
	
	def __updateDocument(self, processPath, group, id, path, value, action, defaultPath=None, checksum=None):
		# get collection name and key from id
		idDict = self.__splitId(id)
		# get processor
		processor = json.loads(open(processPath).read())		
		# get value type
		schemaFragment = self.__getSchemaFragment(idDict["collection"], path)
		schemaType = schemaFragment["type"]
		# check if editable
		checkPath = (path if defaultPath == None else defaultPath)
		propertyDef = self.__checkPropertyEditable(processor, group, checkPath, id)	
		# get current document
		curDoc = self.DB.AQLQuery("LET d = DOCUMENT('%s') RETURN UNSET(d, '_id', '_key', '_rev')" % (id), rawResults=True, batchSize=self.batchSize)[0]
		# get value
		jsonpathExp = parse("$." + path)
		data = jsonpathExp.find(curDoc)
		# check if data
		if len(data) != 1:
			raise DBCreateError("No or too many corresponding data found")
		# set first value as data
		data = data[0]
		# get last path part
		lastPart = self.__getLastPathPart(path, data.context.value)
		# edit according to action
		if action == 'e' and isinstance(value, str):

			# TODO handle special types (file upload, siehe oben...)

			# check process group for edit mode
			if "editMode" in propertyDef:
				# process edit mode	
				self.__processEditMode(propertyDef["editMode"], value, schemaType, data, lastPart, checksum)
			else:
				# change value
				data.context.value[lastPart] = value
		elif action == 'a':
			# check if already exists
			if value in data.value:
				raise DBCreateError("This data already exists")
			# add value if exists
			data.context.value[lastPart].append(value)
		elif action == 'r':
			if value not in data.value:
				raise DBCreateError("Value does not exist")
			# remove value if exists
			data.context.value[lastPart].remove(value)
		else:
			raise DBCreateError("Data type does not correspond to update action")
		# validate
		self.validate(curDoc, "%s.schema.json" % (idDict["collection"]))
		# set altered data and update
		doc = self.DB[idDict["collection"]][idDict["key"]]
		doc.set(curDoc)
		doc.save()
	
	def __checkEdgeCollection(self, graph, collectionName, mainId, linkedId, direction=None):
		# prepare id dictionary
		ids = {}
		# check if direction given
		if direction is not None:
			if direction == "toMain":
				ids["from"] = linkedId
				ids["to"] = mainId
			elif direction == "fromMain":
				ids["from"] = mainId
				ids["to"] = linkedId
			else:
				raise EdgeExistsError("'%s' is not a valid direction" % (direction))
		# if no direction
		else:
			# get definition of edge collection
			edgeDefinition = graph.definitions[collectionName]
			# get froma and to collections
			fromCollections = edgeDefinition.fromCollections
			toCollections = edgeDefinition.toCollections
			# split from and to ids into collection and key
			mainIdColl = self.__splitId(mainId)["collection"]
			linkedIdColl = self.__splitId(linkedId)["collection"]		
			# assign from and to collections
			if mainIdColl in fromCollections:
				ids["from"] = mainId
			elif mainIdColl in toCollections:
				ids["to"] = mainId
			else:
				raise EdgeExistsError("First collection '%s' does not exist in this edge definition" % (mainIdColl))
			if linkedIdColl in fromCollections:
				ids["from"] = linkedId
			elif linkedIdColl in toCollections:
				ids["to"] = linkedId
			else:
				raise EdgeExistsError("Second collection '%s' does not exist in this edge definition" % (mainIdColl))
		# return from and to id
		return ids
	
	def __getDocuments(self, graph, groupQuery, fields, docId=None, properties=False):
		# prepare main filter add
		mainFilterAdd = False
		# get entry point query if exists
		if "entries" in groupQuery:
			entryPoints = self.__createEntryPoints(groupQuery["entries"], graph, fields, None)
			# assemble entry point query
			queryStatement = "%s FOR c_v, e IN 1..1 ANY c_%s GRAPH '%s' FILTER IS_SAME_COLLECTION(e, '%s')" % (entryPoints["query"], entryPoints["var"], graph, groupQuery["main"]["edge"])
			# add document id to filter
			if docId is not None:
				queryStatement += " && c_v._id == '%s'" % (docId)
			# set main filter add true
			mainFilterAdd = True
		# otherwise make direct query
		else:
			queryStatement = "FOR c_v IN %s" % (groupQuery["main"]["collection"])
			# add document id to filter
			if docId is not None:
				queryStatement += " FILTER c_v._id == '%s'" % (docId)
				# set main filter add true
				mainFilterAdd = True
		# create main filters
		mainFilter = self.__createFilters(groupQuery["main"], "v", fields, None, (mainFilterAdd))
		# create properties
		if properties:
			returnStatement = "{'id': c_v._id, 'data': REMOVE_VALUE([c_v." + ", c_v.".join(groupQuery["main"]["properties"]) + "], null)}"
		else:
			returnStatement = "c_v"
		# assemble query
		query = "%s %s RETURN %s" % (queryStatement, mainFilter, returnStatement)
		# query DB and return result
		return self.DB.AQLQuery(query, rawResults=True, batchSize=self.batchSize)
	
	def __checkCorrectDocument(self, graph, groupQuery, fields, id):
		# get the document via its id
		result = self.__getDocuments(graph, groupQuery, fields, id)
		if len(result) == 0:
			raise InvalidDataError("The document with id '%s' cannot be linked (not existing or not allowed)" % (id))
