const jsdom = require('jsdom');
const { JSDOM } = jsdom;

const dom = new JSDOM('<div id="editorOld"></div><div id="editorNew"></div>');

dom.window.document.getSelection = function() { return { getRangeAt: function() { } }; };
dom.window.document.execCommand = function (command, showUI, value) { try { return document.execCommand(command, showUI, value); } catch(e) {} return false; };

global.window = dom.window;
global.document = dom.window.document;
global.Node = dom.window.Node;
global.navigator = global.window.navigator;
global.Text = dom.window.Text;
global.HTMLElement = window.HTMLElement;
global.MutationObserver = dom.window.MutationObserver;

// console.log(process.argv);

// parse old value
try
{
	var oldValue = JSON.parse(process.argv[2]);
}
catch (error)
{
	console.log(error);
	process.exit(1);
}

// parse new value
try
{
	var newValue = JSON.parse(process.argv[3]);
}
catch (error)
{
	console.log(error);
	process.exit(2);
}

let Quill = require('quill');

var quillOld = new Quill('#editorOld');
quillOld.setContents(oldValue);
// console.log(quillOld.getText());

var quillNew = new Quill('#editorNew');
quillNew.setContents(newValue);
// console.log(quillNew.getText());

function findDiff(oldContent, newContent) {
  var diff = oldContent.diff(newContent);

  for (var i = 0; i < diff.ops.length; i++) {
		var op = diff.ops[i];
		// if the change was an insertion
		if (op.hasOwnProperty('insert')) {
			// color it green
			op.attributes = {
			background: "#cce8cc",
			color: "#003700"
			};
		}
		// if the change was a deletion 
		if (op.hasOwnProperty('delete')) {
			// keep the text
			op.retain = op.delete;
			delete op.delete;
			// but color it red and struckthrough
			op.attributes = {
			background: "#e8cccc",
			color: "#370000",
			strike: true
			};
		}
  }

  return oldContent.compose(diff);
}

var diff = findDiff(quillOld.getContents(), quillNew.getContents());

console.log(JSON.stringify(diff));
process.exit(0);
